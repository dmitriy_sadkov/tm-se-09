package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {
    @Nullable
    User findOne(@NotNull final String userId);

    @Nullable
    User findByLogin(@NotNull final String login);

    @NotNull
    List<User> findAll();

    void removeAll();

    void update(@NotNull final String userId, @NotNull final String login);

    void removeByLogin(@NotNull final String login);
}
