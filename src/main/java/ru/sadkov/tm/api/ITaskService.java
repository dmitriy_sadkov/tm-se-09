package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {
    @NotNull
    ITaskRepository getTaskRepository();

    void setTaskRepository(@NotNull final ITaskRepository taskRepository);

    @Nullable
    List<Task> getSortedTaskList(@Nullable final String userId, @Nullable final Comparator<Task> comparator);

    @Nullable
    Task findTaskByName(@Nullable final String taskName, @Nullable final String userId);

    boolean saveTask(@Nullable final String taskName, @Nullable final String projectName, @Nullable final User currentUser);

    void removeTask(@Nullable final String taskName, @Nullable final User currentUser);

    @Nullable
    List<Task> findAll(@Nullable final User user);

    void removeAll(@Nullable final User user);

    void removeTaskForProject(@Nullable final String projectName, @Nullable final User currentUser);

    void update(@Nullable final String oldName, @Nullable final String newName, @Nullable final String userId);

    @Nullable
    List<Task> getTasksByPart(@Nullable final String userId, @Nullable final String part);

    @Nullable
    List<Task> findTasksByStatus(@Nullable final String userId, @Nullable final Status status);

    @Nullable
    String startTask(@Nullable final String userId, @Nullable final String taskName);

    @Nullable
    String endTask(@Nullable final String userId, @Nullable final String taskName);
}
