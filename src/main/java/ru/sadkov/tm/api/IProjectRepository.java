package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {
    @NotNull
    Collection<Project> findAll(@NotNull final String userId);

    void remove(@NotNull final String userId, @NotNull final String projectName);

    void removeAll(@NotNull final String userId);

    @Nullable
    Project findOne(@NotNull final String projectId, @NotNull final String userId);

    boolean merge(@NotNull final String projectName, @NotNull final String userId, @NotNull final String description);

    void update(@NotNull final String userId, @NotNull final String id, @NotNull final String description, @NotNull final String projectName);

    @Nullable
    String findProjectIdByName(@NotNull final String name, @NotNull final String userId);

    @NotNull
    List<Project> findProjectsByPart(@NotNull final String userId, @NotNull final String part);

    @Nullable
    Date startProject(@NotNull final String userId, @NotNull final String projectName);

    @Nullable
    Date endProject(@NotNull final String userId, @NotNull final String projectName);

    @NotNull
    List<Project> findProjectsByStatus(@NotNull final String userId, @NotNull final Status status);
}
