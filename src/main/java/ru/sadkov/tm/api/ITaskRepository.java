package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.enumeration.Status;

import java.util.Date;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {
    @NotNull
    List<Task> findAll(@NotNull final String userId);

    void removeAll(@NotNull final String userId);

    @Nullable
    Task findOne(@NotNull final String taskId, @NotNull final String userId);

    void merge(@NotNull final String taskName, @NotNull final String projectId, @NotNull final String userId);

    void update(@NotNull final String taskId, @NotNull final String taskName, @NotNull final String userId);

    @Nullable
    Task findTaskByName(@NotNull final String taskName, @NotNull final String userId);

    void removeByName(@NotNull final String taskName, @NotNull final String userId);

    @NotNull
    List<Task> getTasksByPart(@NotNull final String userId, @NotNull final String part);

    @NotNull
    List<Task> findTasksByStatus(@NotNull final String userId, @NotNull final Status status);

    @Nullable
    Date startTask(@NotNull final String userId, @NotNull final String taskName);

    @Nullable
    Date endTask(@NotNull final String userId, @NotNull final String taskName);
}
