package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.User;


import java.security.NoSuchAlgorithmException;

public interface IUserService {
    @Nullable
    User getCurrentUser();

    void setCurrentUser(@Nullable final User currentUser);

    void userRegister(@Nullable final User user) throws NoSuchAlgorithmException;

    boolean login(@Nullable final String login, @Nullable final String password) throws NoSuchAlgorithmException;

    void logout();

    void updatePassword(@Nullable final String newPassword) throws NoSuchAlgorithmException;

    @Nullable
    User findOne();

    boolean updateProfile(@Nullable final String newUserName);

    void addTestUsers() throws NoSuchAlgorithmException;

    boolean isAuth();
}
