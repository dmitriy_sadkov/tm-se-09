package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.repository.ProjectRepository;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {
    @Nullable
    String findProjectIdByName(@Nullable final String projectName, @Nullable final String userId);

    boolean persist(@Nullable final String projectName, @Nullable final String userId, @Nullable final String description);

    void removeByName(@Nullable final String projectName, @Nullable final String userId);

    @Nullable
    List<Project> findAll(@Nullable final User user);

    @Nullable
    List<Project> getSortedProjectList(@Nullable final String userId, @Nullable final Comparator<Project> comparator);

    void removeAll(@Nullable final User user);

    @NotNull
    IProjectRepository getProjectRepository();

    void setProjectRepository(@NotNull final ProjectRepository projectRepository);

    void update(@Nullable final Project project, @Nullable final String projectName, @Nullable final String description);

    @Nullable
    Project findOneByName(@Nullable final String projectName, @Nullable final User currentUser);

    @Nullable
    List<Project> findProjectsByPart(@Nullable final String id, @Nullable final String part);

    @Nullable
    List<Project> findProjectsByStatus(@Nullable final String userId, @Nullable final Status status);

    @Nullable
    String startProject(@Nullable final String userId, @Nullable final String projectName);

    @Nullable
    String endProject(@Nullable final String userId, @Nullable final String projectName);

}
