package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectRepository;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.util.RandomUtil;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public void persist(@NotNull final Project project) {
        entities.put(project.getId(), project);
    }

    public void remove(@NotNull final String userId, @NotNull final String projectName) {
        entities.values().removeIf(nextProject -> (nextProject.getName().equalsIgnoreCase(projectName)
                && (nextProject.getUserId().equals(userId))));
    }

    @NotNull
    @Override
    public Collection<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (Project project : entities.values()) {
            if (project.getUserId().equals(userId)) {
                result.add(project);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public String findProjectIdByName(@NotNull final String name, @NotNull final String userId) {
        for (Project project : findAll(userId)) {
            if (project.getName().equals(name)) {
                return project.getId();
            }
        }
        return null;
    }

    public void removeAll(@NotNull final String userId) {
        entities.values().removeIf(nextProject -> nextProject.getUserId().equals(userId));
    }

    @Nullable
    public Project findOne(@NotNull final String projectId, @NotNull final String userId) {
        for (Project project : findAll(userId)) {
            if (project.getId().equals(projectId)) {
                return project;
            }
        }
        return null;
    }

    public boolean merge(@NotNull final String projectName, @NotNull final String userId, @NotNull final String description) {
        @Nullable final String projectId = findProjectIdByName(projectName, userId);
        if (projectId == null || projectId.isEmpty()) {
            persist(new Project(projectName, RandomUtil.UUID(), userId, description));
            return true;
        }
        update(userId, projectId, description, projectName);
        return true;
    }

    public void update(@NotNull final String userId, @NotNull final String id, @NotNull final String description, @NotNull final String projectName) {
        @Nullable final Project project = findOne(id, userId);
        if (project == null) return;
        project.setName(projectName);
        project.setDescription(description);
    }

    @Override
    @NotNull
    public List<Project> findProjectsByPart(@NotNull final String userId, @NotNull final String part) {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (@NotNull final Project project : findAll(userId)) {
            if (project.getDescription() == null || project.getDescription().isEmpty()) {
                if (project.getName().contains(part)) {
                    projects.add(project);
                }
            } else if (project.getName().contains(part) || project.getDescription().contains(part)) {
                projects.add(project);
            }
        }
        return projects;
    }


    @Override
    @NotNull
    public List<Project> findProjectsByStatus(@NotNull final String userId, @NotNull final Status status) {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (@NotNull final Project project : findAll(userId)) {
            if (project.getStatus().equals(status)) {
                projects.add(project);
            }
        }
        return projects;
    }

    @Override
    @Nullable
    public Date startProject(@NotNull final String userId, @NotNull final String projectName) {
        @NotNull final Date startDate = new Date();
        @Nullable final String projectId = findProjectIdByName(projectName, userId);
        if (projectId == null || projectId.isEmpty()) return null;
        @Nullable final Project project = findOne(projectId, userId);
        if (project == null || !project.getStatus().equals(Status.PLANNED)) return null;
        project.setStatus(Status.PROCESS);
        project.setDateBegin(startDate);
        return startDate;
    }

    @Override
    @Nullable
    public Date endProject(@NotNull final String userId, @NotNull final String projectName) {
        @NotNull final Date endDate = new Date();
        @Nullable final String projectId = findProjectIdByName(projectName, userId);
        if (projectId == null || projectId.isEmpty()) return null;
        @Nullable final Project project = findOne(projectId, userId);
        if (project == null || !project.getStatus().equals(Status.PROCESS)) return null;
        project.setStatus(Status.DONE);
        project.setDateEnd(endDate);
        return endDate;
    }
}
