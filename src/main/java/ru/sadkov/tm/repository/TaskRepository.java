package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.util.RandomUtil;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ru.sadkov.tm.api.ITaskRepository {
    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> result = new ArrayList<>();
        for (Task task : entities.values()) {
            if (task.getUserId().equals(userId)) {
                result.add(task);
            }
        }
        return result;
    }

    public void persist(@NotNull final Task task) {
        entities.put(task.getId(), task);
    }

    public void removeByName(@NotNull final String taskName, @NotNull final String userId) {
        entities.values().removeIf(nextTask -> nextTask.getName().equalsIgnoreCase(taskName) &&
                nextTask.getUserId().equals(userId));
    }

    public void removeAll(@NotNull final String userId) {
        entities.values().removeIf(nextTask -> nextTask.getUserId().equals(userId));
    }

    @Nullable
    public Task findOne(@NotNull final String taskId, @NotNull final String userId) {
        for (Task task : findAll(userId)) {
            if (task.getId().equals(taskId)) {
                return task;
            }
        }
        return null;
    }

    public void merge(@NotNull final String taskName, @NotNull final String projectId, @NotNull final String userId) {
        @Nullable final Task task = findTaskByName(taskName, userId);
        if (task == null) {
            persist(new Task(taskName, RandomUtil.UUID(), projectId, userId));
            return;
        }
        update(taskName, taskName, userId);
    }

    public void update(@NotNull final String oldName, @NotNull final String newName, @NotNull final String userId) {
        @Nullable final Task task = findTaskByName(oldName, userId);
        if (task == null) return;
        task.setName(newName);
    }

    @Nullable
    public Task findTaskByName(@NotNull final String taskName, @NotNull final String userId) {
        for (Task task : findAll(userId)) {
            if (taskName.equals(task.getName())) {
                return task;
            }
        }
        return null;
    }

    @Override
    @NotNull
    public List<Task> getTasksByPart(@NotNull String userId, @NotNull String part) {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull final Task task : findAll(userId)) {
            if (task.getDescription() == null || task.getDescription().isEmpty()) {
                if (task.getName().contains(part)) {
                    tasks.add(task);
                }
            } else if (task.getName().contains(part) || task.getDescription().contains(part)) {
                tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    @NotNull
    public List<Task> findTasksByStatus(@NotNull final String userId, @NotNull final Status status) {
        @NotNull final List<Task> tasks = new ArrayList<>();
        for (@NotNull final Task task : findAll(userId)) {
            if (task.getStatus().equals(status)) {
                tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    @Nullable
    public Date startTask(@NotNull final String userId, @NotNull final String taskName) {
        @NotNull final Date startDate = new Date();
        @Nullable final Task task = findTaskByName(taskName, userId);
        if (task == null || !task.getStatus().equals(Status.PLANNED)) return null;
        task.setStatus(Status.PROCESS);
        task.setDateBegin(startDate);
        return startDate;
    }

    @Override
    @Nullable
    public Date endTask(@NotNull final String userId, @NotNull final String taskName) {
        @NotNull final Date endDate = new Date();
        @Nullable final Task task = findTaskByName(taskName, userId);
        if (task == null || !task.getStatus().equals(Status.PROCESS)) return null;
        task.setStatus(Status.DONE);
        task.setDateBegin(endDate);
        return endDate;
    }
}
