package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IUserRepository;
import ru.sadkov.tm.entity.User;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public void persist(@NotNull final User user) {
        entities.put(user.getId(), user);
    }

    public User findOne(@NotNull final String userId) {
        return entities.get(userId);
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
        @NotNull final List<User> userList = new ArrayList<>(entities.values());
        for (User user : userList) {
            if (user.getLogin().equals(login))
                return user;
        }
        return null;
    }

    @NotNull
    public List<User> findAll() {
        return new ArrayList<>(entities.values());
    }

    public void removeByLogin(@NotNull final String login) {
        @Nullable final User toDelete = findByLogin(login);
        if (toDelete == null) return;
        entities.remove(toDelete.getId());
    }

    public void removeAll() {
        entities.clear();
    }

    public void update(@NotNull final String userId, @NotNull final String login) {
        entities.get(userId).setLogin(login);
    }

}
