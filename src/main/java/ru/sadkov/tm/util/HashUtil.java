package ru.sadkov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@UtilityClass
public final class HashUtil {
    @Nullable
    public static String hashMD5(@Nullable final String password) throws NoSuchAlgorithmException {
        if (password == null || password.isEmpty()) return null;
        final byte[] bytePassword = password.getBytes(StandardCharsets.UTF_8);
        return new String(MessageDigest.getInstance("MD5").digest(bytePassword));
    }
}
