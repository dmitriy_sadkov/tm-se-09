package ru.sadkov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.util.RandomUtil;

@Getter
@Setter
public final class User extends AbstractEntity {
    private String login;
    private String password;
    private Role role;

    public User(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.id = RandomUtil.UUID();
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", role=" + role +
                '}';
    }
}
