package ru.sadkov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.sadkov.tm.enumeration.Status;

import java.util.Date;

@Getter
@Setter
public final class Task extends AbstractEntity {
    private String name;
    private String description;
    private Date dateCreate;
    private Date dateBegin;
    private Date dateEnd;
    private String projectId;
    private String userId;
    private Status status;

    public Task(String name, String id, String projectId, String userId, String description) {
        this.name = name;
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
        this.description = description;
        this.dateCreate = new Date();
        this.status = Status.PLANNED;
    }

    public Task(String name, String id, String projectId, String userId) {
        this.name = name;
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
        this.description = "description will be here";
        this.dateCreate = new Date();
        this.status = Status.PLANNED;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateCreate=" + dateCreate +
                ", dateBegin=" + dateBegin +
                ", dateEnd=" + dateEnd +
                ", projectId='" + projectId + '\'' +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", id='" + id + '\'' +
                '}';
    }
}
