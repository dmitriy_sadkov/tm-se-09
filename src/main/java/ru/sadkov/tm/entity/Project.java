package ru.sadkov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.sadkov.tm.enumeration.Status;

import java.util.Date;

@Getter
@Setter
public final class Project extends AbstractEntity {
    private String name;
    private String description;
    private Date dateCreate;
    private Date dateBegin;
    private Date dateEnd;
    private String userId;
    private Status status;

    public Project(String name, String id, String userId, String description) {
        this.name = name;
        this.id = id;
        this.userId = userId;
        this.description = description;
        this.dateCreate = new Date();
        this.status = Status.PLANNED;
    }

    public Project(String name, String id, String userId) {
        this.name = name;
        this.id = id;
        this.userId = userId;
        this.description = "Description will be here";
        this.dateCreate = new Date();
        this.status = Status.PLANNED;
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateCreate=" + dateCreate +
                ", dateBegin=" + dateBegin +
                ", dateEnd=" + dateEnd +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", id='" + id + '\'' +
                '}';
    }
}
