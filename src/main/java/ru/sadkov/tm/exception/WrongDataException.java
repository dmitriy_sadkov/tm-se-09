package ru.sadkov.tm.exception;

public class WrongDataException extends Exception {
    public WrongDataException() {
        super("[YOU ENTER WRONG DATA]");
    }

    public WrongDataException(String message) {
        super(message);
    }
}
