package ru.sadkov.tm.command;

import ru.sadkov.tm.api.ServiceLocator;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public AbstractCommand(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public ServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

    public abstract boolean safe();
}
