package ru.sadkov.tm.command.user;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.exception.WrongDataException;

public final class UserUpdatePasswordCommand extends AbstractCommand {
    @Override
    public String command() {
        return "update-password";
    }

    @Override
    public String description() {
        return "Update password of current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PASSWORD UPDATE]");
        if (serviceLocator.getUserService().getCurrentUser() == null)
            throw new WrongDataException("[NO USER! PLEASE LOGIN]");
        System.out.println("[ENTER NEW PASSWORD]");
        final String newPassword = serviceLocator.getScanner().nextLine();
        serviceLocator.getUserService().updatePassword(newPassword);
        System.out.println("[PASSWORD CHANGED]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
