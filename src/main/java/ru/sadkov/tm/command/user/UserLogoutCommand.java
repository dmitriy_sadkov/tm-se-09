package ru.sadkov.tm.command.user;

import ru.sadkov.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {
    @Override
    public String command() {
        return "logout";
    }

    @Override
    public String description() {
        return "End user session";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        serviceLocator.getUserService().logout();
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
