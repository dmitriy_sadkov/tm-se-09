package ru.sadkov.tm.command.user;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.exception.WrongDataException;

public final class UserUpdateInformationCommand extends AbstractCommand {
    @Override
    public String command() {
        return "update-profile";
    }

    @Override
    public String description() {
        return "Updating User profile";
    }

    @Override
    public void execute() throws WrongDataException {
        System.out.println("[UPDATING PROFILE]");
        if (serviceLocator.getUserService().getCurrentUser() == null)
            throw new WrongDataException("[NO USER! PLEASE LOGIN]");
        System.out.println("[ENTER NEW LOGIN]");
        final String newUserName = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getUserService().updateProfile(newUserName)) {
            System.out.println("[SUCCESS]");
            return;
        }
        throw new WrongDataException("[SOMETHING WRONG! TRY AGAIN]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
