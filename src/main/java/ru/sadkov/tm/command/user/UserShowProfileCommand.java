package ru.sadkov.tm.command.user;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.exception.WrongDataException;

public final class UserShowProfileCommand extends AbstractCommand {
    @Override
    public String command() {
        return "show-user";
    }

    @Override
    public String description() {
        return "Show current User profile";
    }

    @Override
    public void execute() throws WrongDataException {
        System.out.println("[CURRENT USER]");
        if (serviceLocator.getUserService().getCurrentUser() == null)
            throw new WrongDataException("[NO USER! PLEASE LOGIN]");
        final User currentUser = serviceLocator.getUserService().findOne();
        System.out.println(currentUser);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
