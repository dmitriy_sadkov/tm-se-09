package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.comparator.ProjectDateEndComparator;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class ProjectSortByDateEnd extends AbstractCommand {
    @Override
    public String command() {
        return "project-sort-by-endDate";
    }

    @Override
    public String description() {
        return "show projects sorted by end date";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS SORTED BY END DATE]");
        if (serviceLocator.getUserService().getCurrentUser() == null) throw new WrongDataException();
        @Nullable final List<Project> projects = serviceLocator.getProjectService()
                .getSortedProjectList(serviceLocator.getUserService().getCurrentUser().getId(), new ProjectDateEndComparator());
        ListShowUtil.showList(projects);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
