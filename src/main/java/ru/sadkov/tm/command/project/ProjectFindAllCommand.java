package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class ProjectFindAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public void execute() throws WrongDataException {
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getCurrentUser());
        if (projectList == null || projectList.isEmpty()) throw new WrongDataException("NO PROJECTS");
        System.out.println("[PROJECTS:]");
        ListShowUtil.showList(projectList);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
