package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class ProjectFindByPart extends AbstractCommand {
    @Override
    public String command() {
        return "project-find-by-part";
    }

    @Override
    public String description() {
        return "Find project by part of name or description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT SEARCH]");
        if (serviceLocator.getUserService().getCurrentUser() == null) throw new WrongDataException("[NO USER]");
        System.out.println("[ENTER PART OF NAME OR DESCRIPTION]");
        @Nullable final String part = serviceLocator.getScanner().nextLine();
        if (part == null || part.isEmpty()) throw new WrongDataException("INVALID NAME OR DESCRIPTION");
        @Nullable final List<Project> projectList = serviceLocator.getProjectService()
                .findProjectsByPart(serviceLocator.getUserService().getCurrentUser().getId(), part);
        ListShowUtil.showList(projectList);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
