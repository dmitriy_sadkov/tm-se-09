package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.comparator.TaskDateCreateComparator;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class TaskSortByDateCreate extends AbstractCommand {
    @Override
    public String command() {
        return "task-sort-by-create";
    }

    @Override
    public String description() {
        return "show tasks sorted by create date";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS SORTED BY CREATE DATE]");
        if (serviceLocator.getUserService().getCurrentUser() == null) throw new WrongDataException();
        @Nullable final List<Task> tasks = serviceLocator.getTaskService()
                .getSortedTaskList(serviceLocator.getUserService().getCurrentUser().getId(), new TaskDateCreateComparator());
        ListShowUtil.showList(tasks);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
