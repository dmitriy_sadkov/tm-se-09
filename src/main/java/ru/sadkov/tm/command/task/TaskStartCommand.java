package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class TaskStartCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-start";
    }

    @Override
    public String description() {
        return "Start task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START TASK]");
        System.out.println("[CHOOSE TASK TO START]");
        if (serviceLocator.getUserService().getCurrentUser() == null) throw new WrongDataException("NO USER");
        @Nullable final List<Task> tasks = serviceLocator.getTaskService()
                .findTasksByStatus(serviceLocator.getUserService().getCurrentUser().getId(), Status.PLANNED);
        if (tasks == null || tasks.isEmpty()) throw new WrongDataException("[NO PLANNED TASK]");
        ListShowUtil.showList(tasks);
        @Nullable final String taskName = serviceLocator.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty()) throw new WrongDataException("[WRONG TASK NAME]");
        @Nullable final String startDate = serviceLocator.getTaskService()
                .startTask(serviceLocator.getUserService().getCurrentUser().getId(), taskName);
        if (startDate == null || startDate.isEmpty())
            throw new WrongDataException("[ERROR! CANT'T START TASK]");
        System.out.println("[TASK: " + taskName.toUpperCase() + " STARTS]");
        System.out.println(("[START DATE: " + startDate + " ]"));
    }

    @Override
    public boolean safe() {
        return false;
    }
}
