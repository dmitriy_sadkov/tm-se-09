package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class TaskFindAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "show-all-task";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws WrongDataException {
        System.out.println("[TASKS:]");
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getCurrentUser());
        if (taskList == null || taskList.isEmpty()) throw new WrongDataException("[NO TASKS]");
        ListShowUtil.showList(taskList);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
