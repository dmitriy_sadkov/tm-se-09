package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;

public final class TaskRemoveAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        serviceLocator.getTaskService().removeAll(serviceLocator.getUserService().getCurrentUser());
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
