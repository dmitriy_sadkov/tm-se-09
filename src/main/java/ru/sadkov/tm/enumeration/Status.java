package ru.sadkov.tm.enumeration;

public enum Status {
    PLANNED("запланировано"),
    PROCESS("в процессе"),
    DONE("готово");

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
