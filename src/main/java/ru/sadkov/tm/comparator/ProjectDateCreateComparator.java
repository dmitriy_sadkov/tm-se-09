package ru.sadkov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.entity.Project;

import java.util.Comparator;

public final class ProjectDateCreateComparator implements Comparator<Project> {

    @Override
    public int compare(@NotNull final Project project1, @NotNull final Project project2) {
        if (project1.getDateCreate() == null) return Integer.MIN_VALUE;
        if (project2.getDateCreate() == null) return Integer.MAX_VALUE;
        final int result = project1.getDateCreate().compareTo(project2.getDateCreate());
        if (result == 0) return project1.getId().compareTo(project2.getId());
        return result;
    }
}
