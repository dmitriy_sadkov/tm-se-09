package ru.sadkov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.entity.Task;

import java.util.Comparator;

public final class TaskDateCreateComparator implements Comparator<Task> {
    @Override
    public int compare(@NotNull final Task task1, @NotNull final Task task2) {
        if (task1.getDateCreate() == null) return Integer.MIN_VALUE;
        if (task2.getDateCreate() == null) return Integer.MAX_VALUE;
        final int result = task1.getDateCreate().compareTo(task2.getDateCreate());
        if (result == 0) return task1.getId().compareTo(task2.getId());
        return result;
    }
}
