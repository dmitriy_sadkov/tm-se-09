package ru.sadkov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.entity.Task;

import java.util.Comparator;

public final class TaskStatusComparator implements Comparator<Task> {
    @Override
    public int compare(@NotNull final Task task1, @NotNull final Task task2) {
        int result = task1.getStatus().compareTo(task2.getStatus());
        if (result == 0) return task1.getId().compareTo(task2.getId());
        return result;
    }
}
